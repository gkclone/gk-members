<?php
/**
 * @file
 * gk_members.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function gk_members_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_user_account_info|user|user|form';
  $field_group->group_name = 'group_user_account_info';
  $field_group->entity_type = 'user';
  $field_group->bundle = 'user';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Account information',
    'weight' => '0',
    'children' => array(
      0 => 'field_user_first_name',
      1 => 'field_user_last_name',
      2 => 'field_user_title',
      3 => 'field_user_comms_opt_in_brand',
      4 => 'field_user_comms_opt_in_other',
      5 => 'field_user_postcode',
      6 => 'field_user_date_of_birth',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Account information',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => '',
        'description' => '',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_user_account_info|user|user|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_user_account|user|user|form';
  $field_group->group_name = 'group_user_account';
  $field_group->entity_type = 'user';
  $field_group->bundle = 'user';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Account details',
    'weight' => '1',
    'children' => array(
      0 => 'account',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Account details',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => '',
        'description' => '',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_user_account|user|user|form'] = $field_group;

  return $export;
}
