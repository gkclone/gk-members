<?php

$plugin = array(
  'title' => 'Members: User profile form',
  'category' => 'GK Members',
  'single' => TRUE,
);

function gk_members_gk_members_user_profile_form_content_type_render($subtype, $conf, $args, $context) {
  if (user_is_logged_in()) {
    module_load_include('inc', 'user', 'user.pages');

    global $user;
    $account = user_load($user->uid);

    return (object) array(
      'title' => $conf['override_title'] ? $conf['override_title_text'] : '',
      'content' => drupal_get_form('user_profile_form', $account),
    );
  }
}

function gk_members_gk_members_user_profile_form_content_type_edit_form($form, &$form_state) {
  return $form;
}
