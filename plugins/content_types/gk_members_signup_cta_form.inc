<?php

$plugin = array(
  'title' => 'Members: Sign-up CTA form',
  'category' => 'GK Members',
  'single' => TRUE,
);

function gk_members_gk_members_signup_cta_form_content_type_render($subtype, $conf, $args, $context) {
  return (object) array(
    'title' => $conf['override_title'] ? $conf['override_title_text'] : '',
    'content' => drupal_get_form('gk_members_signup_cta_form'),
  );
}

function gk_members_gk_members_signup_cta_form_content_type_edit_form($form, &$form_state) {
  return $form;
}
