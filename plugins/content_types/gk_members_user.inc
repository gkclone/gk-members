<?php

$plugin = array(
  'title' => 'Members: User',
  'category' => 'GK Members',
  'single' => TRUE,
);

function gk_members_gk_members_user_content_type_render($subtype, $conf, $args, $context) {
  return (object) array(
    'title' => $conf['override_title'] ? $conf['override_title_text'] : '',
    'content' => user_is_anonymous() ? drupal_get_form('user_login_block') : array('#theme' => 'gk_members_user'),
  );
}

function gk_members_gk_members_user_content_type_edit_form($form, &$form_state) {
  return $form;
}
