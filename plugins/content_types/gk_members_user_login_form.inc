<?php

$plugin = array(
  'title' => 'Members: User login form',
  'category' => 'GK Members',
  'single' => TRUE,
);

function gk_members_gk_members_user_login_form_content_type_render($subtype, $conf, $args, $context) {
  if (user_is_anonymous()) {
    return (object) array(
      'title' => $conf['override_title'] ? $conf['override_title_text'] : '',
      'content' => drupal_get_form('user_login'),
    );
  }
}

function gk_members_gk_members_user_login_form_content_type_edit_form($form, &$form_state) {
  return $form;
}
