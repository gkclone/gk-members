<?php

$plugin = array(
  'title' => 'Members: Landing',
  'category' => 'GK Members',
  'single' => TRUE,
);

function gk_members_gk_members_landing_content_type_render($subtype, $conf, $args, $context) {
  global $user;
  $account = user_load($user->uid);

  return (object) array(
    'title' => $conf['override_title'] ? $conf['override_title_text'] : '',
    'content' => user_view_page($account),
  );
}

function gk_members_gk_members_landing_content_type_edit_form($form, &$form_state) {
  return $form;
}
