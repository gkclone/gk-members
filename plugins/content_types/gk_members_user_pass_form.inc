<?php

$plugin = array(
  'title' => 'Members: User pass form',
  'category' => 'GK Members',
  'single' => TRUE,
);

function gk_members_gk_members_user_pass_form_content_type_render($subtype, $conf, $args, $context) {
  module_load_include('inc', 'user', 'user.pages');

  return (object) array(
    'title' => $conf['override_title'] ? $conf['override_title_text'] : '',
    'content' => drupal_get_form('user_pass'),
  );
}

function gk_members_gk_members_user_pass_form_content_type_edit_form($form, &$form_state) {
  return $form;
}
