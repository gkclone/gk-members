<?php
/**
 * @file
 * gk_members.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_pages().
 */
function gk_members_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'gk_members_landing';
  $page->task = 'page';
  $page->admin_title = 'Members: Landing';
  $page->admin_description = '';
  $page->path = 'members';
  $page->access = array(
    'plugins' => array(
      0 => array(
        'name' => 'role',
        'settings' => array(
          'rids' => array(
            0 => 2,
          ),
        ),
        'context' => 'logged-in-user',
        'not' => FALSE,
      ),
    ),
    'logic' => 'and',
    'type' => 'none',
    'settings' => NULL,
  );
  $page->menu = array(
    'type' => 'normal',
    'title' => 'Members',
    'name' => 'user-menu',
    'weight' => '0',
    'parent' => array(
      'type' => 'none',
      'title' => '',
      'name' => 'navigation',
      'weight' => '0',
    ),
  );
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_gk_members_landing_panel_context';
  $handler->task = 'page';
  $handler->subtask = 'gk_members_landing';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
  );
  $display = new panels_display();
  $display->layout = '2_columns_25_75';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'top' => NULL,
      'content_header' => NULL,
      'content_top' => NULL,
      'content' => NULL,
      'content_bottom' => NULL,
      'secondary' => NULL,
      'tertiary' => NULL,
      'bottom' => NULL,
      'primary' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'Members';
  $display->uuid = '952dc955-746b-4244-9999-b76e5c32cbfa';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-1531e524-f7a7-4496-b7f5-2684d78aab45';
    $pane->panel = 'primary';
    $pane->type = 'gk_members_landing';
    $pane->subtype = 'gk_members_landing';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '1531e524-f7a7-4496-b7f5-2684d78aab45';
    $display->content['new-1531e524-f7a7-4496-b7f5-2684d78aab45'] = $pane;
    $display->panels['primary'][0] = 'new-1531e524-f7a7-4496-b7f5-2684d78aab45';
    $pane = new stdClass();
    $pane->pid = 'new-9558c9e7-4dcd-46fd-8dfb-1bf484cf1564';
    $pane->panel = 'secondary';
    $pane->type = 'block';
    $pane->subtype = 'menu_block-gk-members-user-menu';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '9558c9e7-4dcd-46fd-8dfb-1bf484cf1564';
    $display->content['new-9558c9e7-4dcd-46fd-8dfb-1bf484cf1564'] = $pane;
    $display->panels['secondary'][0] = 'new-9558c9e7-4dcd-46fd-8dfb-1bf484cf1564';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['gk_members_landing'] = $page;

  return $pages;

}
