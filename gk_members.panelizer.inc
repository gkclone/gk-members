<?php
/**
 * @file
 * gk_members.panelizer.inc
 */

/**
 * Implements hook_panelizer_defaults().
 */
function gk_members_panelizer_defaults() {
  $export = array();

  $panelizer = new stdClass();
  $panelizer->disabled = FALSE; /* Edit this to true to make a default panelizer disabled initially */
  $panelizer->api_version = 1;
  $panelizer->name = 'node:page:gk_members_login';
  $panelizer->title = 'Members: Login';
  $panelizer->panelizer_type = 'node';
  $panelizer->panelizer_key = 'page';
  $panelizer->no_blocks = FALSE;
  $panelizer->css_id = '';
  $panelizer->css = '';
  $panelizer->pipeline = 'ipe';
  $panelizer->contexts = array();
  $panelizer->relationships = array();
  $panelizer->access = array();
  $panelizer->view_mode = 'page_manager';
  $panelizer->css_class = '';
  $panelizer->title_element = 'H2';
  $panelizer->link_to_entity = TRUE;
  $panelizer->extra = array();
  $display = new panels_display();
  $display->layout = '1_column';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'primary' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '%node:title';
  $display->uuid = 'ab6e7dea-2a38-4f54-bb28-648de495eb08';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-9f867a1c-3f63-45b5-b229-e9c47642e52d';
    $pane->panel = 'primary';
    $pane->type = 'pane_content_header';
    $pane->subtype = 'pane_content_header';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '9f867a1c-3f63-45b5-b229-e9c47642e52d';
    $display->content['new-9f867a1c-3f63-45b5-b229-e9c47642e52d'] = $pane;
    $display->panels['primary'][0] = 'new-9f867a1c-3f63-45b5-b229-e9c47642e52d';
    $pane = new stdClass();
    $pane->pid = 'new-171a8812-535c-44c8-a958-869456668a4c';
    $pane->panel = 'primary';
    $pane->type = 'entity_view';
    $pane->subtype = 'node';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'view_mode' => 'full',
      'context' => 'panelizer',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '171a8812-535c-44c8-a958-869456668a4c';
    $display->content['new-171a8812-535c-44c8-a958-869456668a4c'] = $pane;
    $display->panels['primary'][1] = 'new-171a8812-535c-44c8-a958-869456668a4c';
    $pane = new stdClass();
    $pane->pid = 'new-082b23ac-1fe6-48e1-b757-69f5a3a9a53b';
    $pane->panel = 'primary';
    $pane->type = 'gk_members_user_login_form';
    $pane->subtype = 'gk_members_user_login_form';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = '082b23ac-1fe6-48e1-b757-69f5a3a9a53b';
    $display->content['new-082b23ac-1fe6-48e1-b757-69f5a3a9a53b'] = $pane;
    $display->panels['primary'][2] = 'new-082b23ac-1fe6-48e1-b757-69f5a3a9a53b';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $panelizer->display = $display;
  $export['node:page:gk_members_login'] = $panelizer;

  $panelizer = new stdClass();
  $panelizer->disabled = FALSE; /* Edit this to true to make a default panelizer disabled initially */
  $panelizer->api_version = 1;
  $panelizer->name = 'node:page:gk_members_password';
  $panelizer->title = 'Members: Password';
  $panelizer->panelizer_type = 'node';
  $panelizer->panelizer_key = 'page';
  $panelizer->no_blocks = FALSE;
  $panelizer->css_id = '';
  $panelizer->css = '';
  $panelizer->pipeline = 'ipe';
  $panelizer->contexts = array();
  $panelizer->relationships = array();
  $panelizer->access = array();
  $panelizer->view_mode = 'page_manager';
  $panelizer->css_class = '';
  $panelizer->title_element = 'H2';
  $panelizer->link_to_entity = TRUE;
  $panelizer->extra = array();
  $display = new panels_display();
  $display->layout = '1_column';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'primary' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '%node:title';
  $display->uuid = 'af4beaea-529b-425c-a295-cead769de6a6';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-0a423fa4-2270-4b89-92cc-bf6e8b5ecd3c';
    $pane->panel = 'primary';
    $pane->type = 'pane_content_header';
    $pane->subtype = 'pane_content_header';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '0a423fa4-2270-4b89-92cc-bf6e8b5ecd3c';
    $display->content['new-0a423fa4-2270-4b89-92cc-bf6e8b5ecd3c'] = $pane;
    $display->panels['primary'][0] = 'new-0a423fa4-2270-4b89-92cc-bf6e8b5ecd3c';
    $pane = new stdClass();
    $pane->pid = 'new-833139af-c095-452e-b1fe-f2e5b9d225a0';
    $pane->panel = 'primary';
    $pane->type = 'entity_view';
    $pane->subtype = 'node';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'view_mode' => 'full',
      'context' => 'panelizer',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '833139af-c095-452e-b1fe-f2e5b9d225a0';
    $display->content['new-833139af-c095-452e-b1fe-f2e5b9d225a0'] = $pane;
    $display->panels['primary'][1] = 'new-833139af-c095-452e-b1fe-f2e5b9d225a0';
    $pane = new stdClass();
    $pane->pid = 'new-960754c0-b493-48aa-ba88-7a9b20d384be';
    $pane->panel = 'primary';
    $pane->type = 'gk_members_user_pass_form';
    $pane->subtype = 'gk_members_user_pass_form';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = '960754c0-b493-48aa-ba88-7a9b20d384be';
    $display->content['new-960754c0-b493-48aa-ba88-7a9b20d384be'] = $pane;
    $display->panels['primary'][2] = 'new-960754c0-b493-48aa-ba88-7a9b20d384be';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $panelizer->display = $display;
  $export['node:page:gk_members_password'] = $panelizer;

  $panelizer = new stdClass();
  $panelizer->disabled = FALSE; /* Edit this to true to make a default panelizer disabled initially */
  $panelizer->api_version = 1;
  $panelizer->name = 'node:page:gk_members_profile';
  $panelizer->title = 'Members: Profile';
  $panelizer->panelizer_type = 'node';
  $panelizer->panelizer_key = 'page';
  $panelizer->no_blocks = FALSE;
  $panelizer->css_id = '';
  $panelizer->css = '';
  $panelizer->pipeline = 'ipe';
  $panelizer->contexts = array();
  $panelizer->relationships = array();
  $panelizer->access = array();
  $panelizer->view_mode = 'page_manager';
  $panelizer->css_class = '';
  $panelizer->title_element = 'H2';
  $panelizer->link_to_entity = TRUE;
  $panelizer->extra = array();
  $display = new panels_display();
  $display->layout = '2_columns_25_75';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'primary' => NULL,
      'secondary' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '%node:title';
  $display->uuid = 'aec55234-3e29-40ae-805d-08676c03d839';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-4f60ecc7-a47f-45ca-ad16-a21a9565b42a';
    $pane->panel = 'primary';
    $pane->type = 'pane_content_header';
    $pane->subtype = 'pane_content_header';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '4f60ecc7-a47f-45ca-ad16-a21a9565b42a';
    $display->content['new-4f60ecc7-a47f-45ca-ad16-a21a9565b42a'] = $pane;
    $display->panels['primary'][0] = 'new-4f60ecc7-a47f-45ca-ad16-a21a9565b42a';
    $pane = new stdClass();
    $pane->pid = 'new-f51b6a74-a41b-4f00-b8cb-daef26b76ab0';
    $pane->panel = 'primary';
    $pane->type = 'entity_view';
    $pane->subtype = 'node';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'view_mode' => 'full',
      'context' => 'panelizer',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = 'f51b6a74-a41b-4f00-b8cb-daef26b76ab0';
    $display->content['new-f51b6a74-a41b-4f00-b8cb-daef26b76ab0'] = $pane;
    $display->panels['primary'][1] = 'new-f51b6a74-a41b-4f00-b8cb-daef26b76ab0';
    $pane = new stdClass();
    $pane->pid = 'new-c1dfe97b-d6fe-40c5-8b29-00549d191e05';
    $pane->panel = 'primary';
    $pane->type = 'gk_members_user_profile_form';
    $pane->subtype = 'gk_members_user_profile_form';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = 'c1dfe97b-d6fe-40c5-8b29-00549d191e05';
    $display->content['new-c1dfe97b-d6fe-40c5-8b29-00549d191e05'] = $pane;
    $display->panels['primary'][2] = 'new-c1dfe97b-d6fe-40c5-8b29-00549d191e05';
    $pane = new stdClass();
    $pane->pid = 'new-5568f8d0-198d-4b1f-9c4f-a0a21d73ddf4';
    $pane->panel = 'secondary';
    $pane->type = 'block';
    $pane->subtype = 'menu_block-gk-members-user-menu';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '5568f8d0-198d-4b1f-9c4f-a0a21d73ddf4';
    $display->content['new-5568f8d0-198d-4b1f-9c4f-a0a21d73ddf4'] = $pane;
    $display->panels['secondary'][0] = 'new-5568f8d0-198d-4b1f-9c4f-a0a21d73ddf4';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $panelizer->display = $display;
  $export['node:page:gk_members_profile'] = $panelizer;

  return $export;
}
